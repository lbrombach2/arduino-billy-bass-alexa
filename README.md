# Arduino Billy Bass Alexa

SIMPLE Arduino sketch and diagram for sampling an audio input and moving the mouth when speaking detected. Seriously...why is every sketch or raspberry Pi code one million lines and functions long? No motor shield required.

My version of Billy Bass Alexa uses an arduino R3 and the original Billy Bass motor drivers so I don't need an arduino motor shield. Nor do I bother with pwm...the mouth /head get powered or not - I don't believe in complicating things just because I can. Unfortunately using the original audio driver was not in the cards (the audio amplifier I believe is inside the blop-covered microcontroller that also holds the songs and program) so I took one out of an old computer speaker to drive the origial Bill Bass speaker.

Whole write-up can be found at https://lloydbrombach.wordpress.com/2018/12/26/simplest-hacking-of-billy-bass-singing-fish-to-talk-for-alexa-or-youtube-or-anything-really/

Short sample video at https://youtu.be/He57zYd4TQw