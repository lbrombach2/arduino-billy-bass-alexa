///////////////////////////////////////////////////////////////////////////////////////////
// This Arduino Sketch is to read an audio signal from Alexa and animate a Billy Bass Fish
// when it detects speech. The algorithm could easily work for Raspberry Pi if you add
// an external ADC converter. I chose Arduino because it has one built in. I like simple.
// It Measures and ADC pin and keeps a running average, deciding that reads 25 ADC units
// above average are a syllable. It stores reads in an array and recalculates average 
// regularly - throwing away high readings and lowest readings as possibble errors. 
// A write-up of how the hardware all works and more info can be found at
// lloydbrombach.wordpress.com
///////////////////////////////////////////////////////////////////////////////////////////

const int myADC = 0; 
const int HEAD=12;
const int MOUTH=13;
const int readsCount = 12;

int min=1500;
int max=0;
int avg=analogRead(myADC);

int reads[readsCount]={0};

void setup() {                
  Serial.begin(9600);              //  setup serial
  // initialize the digital pin as an output.
  pinMode(13, OUTPUT); //pin out for Mouth (also LED on uno)
  pinMode(12, OUTPUT); //pin out for head
  pinMode(5, INPUT); //ADC IN (except maybe the pin is actually marked 0?)

  dropHead();
  closeMouth();
 
}/////////end setup()/////////////////
  
///////////////////////////////////////////////loop()////////////////////////////////////////////////////
void loop() {
static int i=0; //main incrementer
static int headDropCount=0;
int current = analogRead(myADC);


///// this section updates the average audio level
///// I seem to get a better effect if I maintain both a running average
///// but also re-calculate the average regularly below
avg=avg*readsCount;
avg-=reads[i];
reads[i]=current;
avg+=reads[i];
avg=avg/readsCount;

closeMouth();  //always start closing mouth to make it look like talking

if (i++ >=readsCount)// re-calculate average and discard mins and maxes as possible noise error
{ 
    min=1500;
    max=0;
    int newSum=0;
   for (int j=0; j<readsCount; j++)
  {
    if (reads[j]<min)min=reads[j];
    if (reads[j]>max)max=reads[j];
    newSum+=reads[j];
  }
  newSum-=min;
  newSum-=max;
  avg=newSum/(readsCount-2);
   i=0;
}

if(current>avg+25) // syllable detected, start animation
{ 
openMouth();
raiseHead();
delay(100);     //this delay could be tweaked for effect

headDropCount=0;
}else{    //no syllable detected, start closing mouth
closeMouth();
headDropCount++;
delay(22);       //this delay could be tweaked for effect
}
if(headDropCount>65) dropHead(); //The head goes up on first syllable, but doesn't drop until interaction is complete

}
///////////end loop()//////////////

///////////animation functions below/////////////
void openMouth() {digitalWrite(MOUTH, HIGH);  }
void closeMouth(){digitalWrite(MOUTH, LOW);  }
void raiseHead() {digitalWrite(HEAD, HIGH);  }
void dropHead(){digitalWrite(HEAD, LOW);  }


  

